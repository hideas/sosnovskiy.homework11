﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sosnovskiy.Homework11
{
    public partial class Scroller : Form
    {
        private List<Button> _btns; // List of buttons

        // Form constructor
        public Scroller()
        {
            InitializeComponent();

            // Adding items to list
            _btns = new List<Button>();
            _btns.Add(btn1);
            _btns.Add(btn2);
            _btns.Add(btn3);

            // Randomizing starting points for buttons
            Random random = new Random();
            btn1.Location = new Point(random.Next(0, 59), random.Next(0, 59));
            btn2.Location = new Point(random.Next(59, 118), random.Next(59, 118));
            btn3.Location = new Point(random.Next(118, 177), random.Next(118, 177));
        }

        // Click method for buttons
        private void btn_Click(object sender, EventArgs e)
        {
            // Get active button
            var btnActive = (Button)sender;
            btnActive.BackColor = Color.Firebrick;

            // If not active - change color back
            foreach (Button btn in _btns)
            {
                if (!btnActive.Equals(btn))
                {
                    btn.BackColor = Color.SteelBlue;
                }
            }
        }

        // Left scroll method for buttons
        private void tbLeft_Scroll(object sender, EventArgs e)
        {
            // Change label text
            lblLeft.Text = $"X: {(tbLeft.Value * 10).ToString()}";

            // Change button position
            foreach (Button btn in _btns)
            {
                if (btn.BackColor.Equals(Color.Firebrick))
                {
                    btn.Left = Convert.ToInt32(tbLeft.Value * 8.85F);
                }
            }
        }

        // Top scroll method for buttons
        private void tbTop_Scroll(object sender, EventArgs e)
        {
            // Change label text
            lblRight.Text = $"Y: {(tbTop.Value * 10).ToString()}";

            // Change button position
            foreach (Button btn in _btns)
            {
                if (btn.BackColor.Equals(Color.Firebrick))
                {
                    btn.Top = 177 - Convert.ToInt32(tbTop.Value * 8.85F);
                }
            }
        }
    }
}
