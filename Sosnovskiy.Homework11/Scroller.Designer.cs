﻿namespace Sosnovskiy.Homework11
{
    partial class Scroller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Scroller));
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.pnlArea = new System.Windows.Forms.Panel();
            this.tbLeft = new System.Windows.Forms.TrackBar();
            this.tbTop = new System.Windows.Forms.TrackBar();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.pnlArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop)).BeginInit();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.SteelBlue;
            this.btn1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn1.Location = new System.Drawing.Point(36, 42);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(20, 20);
            this.btn1.TabIndex = 1;
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.SteelBlue;
            this.btn2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn2.Location = new System.Drawing.Point(105, 42);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(20, 20);
            this.btn2.TabIndex = 2;
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.Firebrick;
            this.btn3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn3.Location = new System.Drawing.Point(94, 125);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(20, 20);
            this.btn3.TabIndex = 0;
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn_Click);
            // 
            // pnlArea
            // 
            this.pnlArea.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlArea.Controls.Add(this.btn1);
            this.pnlArea.Controls.Add(this.btn3);
            this.pnlArea.Controls.Add(this.btn2);
            this.pnlArea.Location = new System.Drawing.Point(1, 47);
            this.pnlArea.Name = "pnlArea";
            this.pnlArea.Size = new System.Drawing.Size(200, 200);
            this.pnlArea.TabIndex = 3;
            // 
            // tbLeft
            // 
            this.tbLeft.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.tbLeft.LargeChange = 1;
            this.tbLeft.Location = new System.Drawing.Point(1, 1);
            this.tbLeft.Maximum = 20;
            this.tbLeft.Name = "tbLeft";
            this.tbLeft.Size = new System.Drawing.Size(200, 45);
            this.tbLeft.TabIndex = 4;
            this.tbLeft.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbLeft.Scroll += new System.EventHandler(this.tbLeft_Scroll);
            // 
            // tbTop
            // 
            this.tbTop.Cursor = System.Windows.Forms.Cursors.HSplit;
            this.tbTop.Location = new System.Drawing.Point(202, 47);
            this.tbTop.Maximum = 20;
            this.tbTop.Name = "tbTop";
            this.tbTop.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tbTop.Size = new System.Drawing.Size(45, 200);
            this.tbTop.TabIndex = 3;
            this.tbTop.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.tbTop.Scroll += new System.EventHandler(this.tbTop_Scroll);
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Location = new System.Drawing.Point(208, 7);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(26, 13);
            this.lblLeft.TabIndex = 5;
            this.lblLeft.Text = "X: 0";
            // 
            // lblRight
            // 
            this.lblRight.AutoSize = true;
            this.lblRight.Location = new System.Drawing.Point(208, 22);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(26, 13);
            this.lblRight.TabIndex = 6;
            this.lblRight.Text = "Y: 0";
            // 
            // Scroller
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(247, 247);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.tbTop);
            this.Controls.Add(this.tbLeft);
            this.Controls.Add(this.pnlArea);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Scroller";
            this.Text = "Scroller";
            this.pnlArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tbLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Panel pnlArea;
        private System.Windows.Forms.TrackBar tbLeft;
        private System.Windows.Forms.TrackBar tbTop;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.Label lblRight;
    }
}

